# Booking API Overview
## Initial Set-up
* Change the MongoDB Atlas database connection string in the backend to ensure connectivity to the correct personal database.

* Import the s32-36.postman_collection.json in your postman application.

## Store the following data in your database
### User credentials (User Registration)
* Admin User
	* firstName : "Admin"
	* lastName : "Admin"
	* email : "admin@mail.com"
	* password : "admin123"
	* isAdmin : true (default value: manually change this one in MongoDB)
	* mobileNo : "09123456789"

* Customer User
	* firstName : "John"
	* lastName : "Smith"
	* email : "johnsmith@mail.com"
	* password : "john123"
	* isAdmin : false (default value)
	* mobileNo : "09123456789"

### Course Creation (Course Request > Create a course)
* Course 1
	* name : "HTML"
	* description : "Learn the Basics of Web Development"
	* price : 1000
	* slots : 20
* Course 2
	* name : "CSS 3"
	* description : "Learn the latest styling standards and web design"
	* price : 2000
	* slots : 20
* Course 3
	* name : "JavaScript"
	* description : "Learn the fundamental concepts of programming using JavaScript"
	* price : 3000
	* slots : 25

### Booking API Features
* User Checking (email)
* User Registration
* User Authentication
* User Profile
* User Enrollment (Non-admin only)
* Course Creation (Admin only)
* Retrieve a single course
* Retrieve all active course
* Retrieve all courses (Admin only)
* Update Course (Admin only)