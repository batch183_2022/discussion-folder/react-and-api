import {useState, useEffect} from "react";
import {Link} from "react-router-dom";
import{Card, Button} from "react-bootstrap";


export default function CourseCard({courseProp}){

	//Check to see if the data was passed successfully
	// passed props is an object within an object
	// Accessing first object
	// console.log(typeof props);
	// console.log(typeof courseProp.name);
	// console.log(props.courseProp.name);

	// Deconstruct courseProp properties 
	const {_id, name, description, price, slots} = courseProp;

	//States 
		//State are used to keep track the information related to individual components.

	//Hooks
		//Special react defined methods and function that allows us to do certain tasks in our components.
		//Use the state hook  for this components to be able to store its state.

	// Syntax
		//const [stateName, setStateName] = useState(initialStateValue)

	//useState() is a hook that creates states.
		//useState() returns an array with 2 items:
			//The first item in the is the state
			//The second one i the setter function (to change the initial state)

		//Array destructuring for the useState()
										//0 can be boolean or any 
		// const [count,setCount] = useState(0);

		/*
			Instructions s46 Activity:
			1. Create a seats state in the CourseCard component and set the initial value to 10.
			2. For every enrollment, deduct one to the seats.
			3. If the seats reaches zero do the following:
			- Do not add to the count.
			- Do not deduct to the seats.
			- Show an alert that says No more seats available.
			5. push to git with the commit message of Add activity code - S46.
			6. Add the link in Boodle.

			const [seats, setSeats] = useState(10);
				// console.log(useState(0));
			function enroll(){
				if(seats > 0){
					setCount(count + 1);
					console.log("Enrolees:" + count);

					setSeats(seats - 1);
					console.log("Seats:", seats)
				}
				else{
					alert("No more seats available");
				}
			}
		*/

	// const [seats, setSeats] = useState(30);
	// const [isOpen, setIsOpen] = useState(false);

	// function enroll(){
	// 		setCount(count + 1);
	// 		// console.log("Enrolees:" + count);

	// 		setSeats(seats - 1);
	// 		// console.log("Seats:", seats);
	// }

	// function unEnroll(){
	// 	setCount(count - 1);
	// }
	// //useEffect()
	// // useEffect allows us to run a task or an effect, the differrence  is that with useEffect
	// useEffect(() =>{
	// 	if(seats === 0){
	// 		alert("No more seats available");
	// 		setIsOpen(true);
	// 	}
	// }, [seats]);

	// If the useEffect() does not have a dependency array, it will run on the initial render and whenever a state is set by its set function.
	// useEffect(()=>{
	// 	//Runs on every render
	// 	console.log("useEffect render");
	// })

	// If the useEffect() has a dependency array but empty it will only run on initial render.
	// useEffect(()=>{
	// 	//Runs only on the initial render
	// 	console.log("useEffect render");
	// }, [])

	// If the useEffect() has dependency array and there is state or data run on it, the useEffect() will run whenever that state is updated.
	// useEffect(()=>{
	// 	//Runs only on the initial render
	// 	// Everytime the dependency value will change (seats).
	// 	console.log("useEffect render");
	// }, [seats, count]) // seats and count are used to call function.

	return(
		<Card className="p-3 my-3">
		    <Card.Body>
		        <Card.Title>
		            {name}
		        </Card.Title>
		        <Card.Subtitle>Description:</Card.Subtitle>
		        <Card.Text>
		           {description}
		        </Card.Text>
		        <Card.Subtitle>Price: </Card.Subtitle>
		        <Card.Text>
		           {price}
		        </Card.Text>
		        <Card.Text>
		           Slots: {slots}
		        </Card.Text>
		    {/*We will be able to select a specific course though its url*/}
		        <Button as={Link} to={`/courses/${_id}`} variant ="primary">Details</Button>
		    </Card.Body>
		</Card>
		)
}