// The "import" statement allows us to use the code/exported modules from other files similar to we use the "require" function in Node JS.
import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

//Import the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
)


// The syntax used in React.js is JSX
// JSX - Javascript + XML
//  - We are able to create HTML elements using JS


// */
// const name = "John Smith";
// const element = <h1>Hello, {name}</h1>

// const user = {
//   firstName: "Jane",
//   lastName: "Smith"
// }
// function formatName(user){
//   return user.firstName + " " + user.lastName;
// }

// const element = <h1>Hello, {formatName(user)}</h1>
// //root.render() - allows to render/display our reactjs elements in out HTML file.
// root.render(element);




/*If you want to start measuring performance in your app, pass a function
to log results (for example: reportWebVitals(console.log))
or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals*/

